#include <Arduino.h>
#include <Keyboard.h>

#include <DueFlashStorage.h>
DueFlashStorage EEPROM;

#include <Keypad.h>
#include <Key.h>

const byte rows = 6;
const byte cols = 16;
char keys[rows][cols] = {
  {'Z','F','A','P','{','{','{','{','{','M','?','>','<','~'},
  {'L','|','z','x','c','v','b','n','m',',','.','/','{','G'},
  {'C','a','s','d','{','f','g','h','j','k','l',';','@','#'},
  {'B','q','w','e','{','r','t','y','u','i','o','p','[',']','N'},
  {'`','1','2','3','{','4','5','6','7','8','9','0','-','=','K'},
  {'E','{','!','R','I','+','$','%','^','_',':','(',')','Q','W','Y'},
};
byte rowPins[rows] = {42, 43, 44, 45, 46, 47}; //connect to the row pinouts of the keypad
byte colPins[cols] = {22, 23, 24, 25, 26, 52, 28, 29, 30, 31, 32, 33, 34, 35, 36,37}; //connect to the column pinouts of the keypad
Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, rows, cols );

int keyboard_state = 0;
int current_led = 1;
int current_led_config_step = 1;
char character = '0';

String val1 = "0";
String val2 = "0";
String val3 = "0";

int tmp_led_pwm_value = 0;

int led_red_pwm_value = 0;
int led_green_pwm_value = 0;
int led_blue_pwm_value = 0;

void setup(){

  //Set the serial up
  //Serial.begin(9300);

  //Serial.println("Initializing...");

  led_red_pwm_value = EEPROM.read(1);
  led_green_pwm_value = EEPROM.read(2);
  led_blue_pwm_value = EEPROM.read(3);

  analogWrite(5, led_red_pwm_value);
  analogWrite(6, led_green_pwm_value);
  analogWrite(7, led_blue_pwm_value);

  /*
  Serial.println("Read RED value: ");
  Serial.println(led_red_pwm_value);
  Serial.println("Read GREEN value: ");
  Serial.println(led_green_pwm_value);
  Serial.println("Read BLUE value: ");
  Serial.println(led_blue_pwm_value);
  */

  Keyboard.begin();
}

void loop(){

  // Returns true if there are ANY active keys.
  if (kpd.getKeys()){

    if(kpd.findInList('>') >= 0 && kpd.findInList('l') >= 0 && keyboard_state == 0){

      //Serial.println("LED Configuration");

      keyboard_state = 1;
    }

    if(keyboard_state == 0){

      //Continue keyboard operations
      for (int i=0; i<LIST_MAX; i++)   // Scan the whole key list.
      {
          if ( kpd.key[i].stateChanged )   // Only find keys that have changed state.
          {
              switch (kpd.key[i].kstate) {  // Report active key state : IDLE, PRESSED, HOLD, or RELEASED
                  case PRESSED:
                  if(kpd.key[i].kchar == 'P'){
                    Keyboard.press(32);
                  }else{
                    Keyboard.press(key_value(kpd.key[i].kchar));
                  }

                  //Serial.println(kpd.key[i].kchar);

              break;
                  case HOLD:
              break;
                  case RELEASED:
                  if(kpd.key[i].kchar == 'P'){
                    Keyboard.release(32);
                  }else{
                    Keyboard.release(key_value(kpd.key[i].kchar));
                  }
              break;
                  case IDLE:
              break;
              }
              //Serial.print("Key ");
              //Serial.print(kpd.key[i].kchar);
          }
      }


    }else if(keyboard_state == 1){

      //LED Configuration

      //Blink the LED's to let the user know
      for(int i = 0; i < 2; i++){

        //Turn all LED's off before running the config
        analogWrite(5, 0);
        analogWrite(6, 0);
        analogWrite(7, 0);

        analogWrite(5, 255);
        delay(400);

        analogWrite(5, 0);
        delay(400);
      }

      while(keyboard_state == 1){

        if (kpd.getKeys()){

          if(kpd.isPressed('E')){

            //Exit the config
            exit_led_config();

            break;
          }

          switch(current_led){

            case 1:
              //Set the RED led colour
              //Serial.println("Configuring the RED Colour");
              set_led(5);
            break;

            case 2:
              //Set the GREEN led colour
              //Serial.println("Configuring the GREEN Colour");
              set_led(6);
            break;

            case 3:
              //Set the BLUE led colour
              //Serial.println("Configuring the BLUE Colour");
              set_led(7);
            break;

            case 4:
            //Save values to EEPROM and exit config
            exit_led_config();
            break;
          }

          if(kpd.isPressed('N')){
            //Move on to the next colour

            //Store the value in EEPROM
            if(current_led == 1){
              EEPROM.write(1, tmp_led_pwm_value);
            }else if(current_led == 2){
              EEPROM.write(2, tmp_led_pwm_value);
            }else if(current_led == 3){
              EEPROM.write(3, tmp_led_pwm_value);
            }

            current_led ++;
            current_led_config_step = 1;
          }
        }
      }
    }

  }
}

void set_led(int pin){

  if(kpd.isPressed('1')){
    character = '1';
  }
  if(kpd.isPressed('2')){
    character = '2';
  }
  if(kpd.isPressed('3')){
    character = '3';
  }
  if(kpd.isPressed('4')){
    character = '4';
  }
  if(kpd.isPressed('5')){
    character = '5';
  }
  if(kpd.isPressed('6')){
    character = '6';
  }
  if(kpd.isPressed('7')){
    character = '7';
  }
  if(kpd.isPressed('8')){
    character = '8';
  }
  if(kpd.isPressed('9')){
    character = '9';
  }
  if(kpd.isPressed('0')){
    character = '0';
  }
  if(kpd.isPressed('G')){
    current_led_config_step ++;
    character = '0';
  }
  if(kpd.isPressed('K')){
    current_led_config_step --;
    character = '0';
  }


  if(current_led_config_step <= 1){

    val1 = character;
    current_led_config_step = 1;
  }

  if(current_led_config_step == 2){

    val2 = character;
  }

  if(current_led_config_step >= 3){

    val3 = character;

    current_led_config_step = 3;
  }

  String val4 = val1 + val2 + val3;

  int val5 = atoi( val4.c_str() );

  if(val5 > 255){
    val5 = 255;
  }else if(val5 < 0){
    val5 = 0;
  }

  delay(200);

  analogWrite(pin, val5);
  tmp_led_pwm_value = val5;
}


//Manage exiting the LED configuration tool
void exit_led_config(){

  //Serial.println("Exiting LED config.");

  //Reset states
  keyboard_state = 0;
  val1 = "0";
  val2 = "0";
  val3 = "0";
  character = '0';
  current_led = 1;
  current_led_config_step = 1;
}

//Convert keys to correct identifiers
char key_value(char key){

  switch(key){
    case 'E':
    return KEY_ESC;
    case '!':
    return KEY_F1;
    case 'R':
    return KEY_F2;
    case '+':
    return KEY_F3;
    case '$':
    return KEY_F4;
    case '^':
    return KEY_F5;
    case '_':
    return KEY_F6;
    case ':':
    return KEY_F7;
    case '(':
    return KEY_F8;
    case ')':
    return KEY_F9;
    case 'Q':
    return KEY_F10;
    case 'W':
    return KEY_F11;
    case 'Y':
    return KEY_F12;
    case 'B':
    return KEY_TAB;
    case 'C':
    return KEY_CAPS_LOCK;
    case 'L':
    return KEY_LEFT_SHIFT;
    case 'Z':
    return KEY_LEFT_CTRL;
    case 'F':
    return KEY_LEFT_GUI;
    case 'A':
    return KEY_LEFT_ALT;
    case 'P':
    return '0x20';
    case 'M':
    return KEY_RIGHT_ALT;
    case '<':
    return KEY_RIGHT_GUI;
    case '~':
    return KEY_RIGHT_CTRL;
    case 'G':
    return KEY_RIGHT_SHIFT;
    case 'N':
    return KEY_RETURN;
    case 'K':
    return KEY_BACKSPACE;

    case '`':
    return '`';
    case '1':
    return '1';
    case '2':
    return '2';
    case '3':
    return '3';
    case '4':
    return '4';
    case '5':
    return '5';
    case '6':
    return '6';
    case '7':
    return '7';
    case '8':
    return '8';
    case '9':
    return '9';
    case '0':
    return '0';
    case '-':
    return '-';
    case '=':
    return '=';
    case 'q':
    return 'q';
    case 'w':
    return 'w';
    case 'e':
    return 'e';
    case 'r':
    return 'r';
    case 't':
    return 't';
    case 'y':
    return 'y';
    case 'u':
    return 'u';
    case 'i':
    return 'i';
    case 'o':
    return 'o';
    case 'p':
    return 'p';
    case '[':
    return '[';
    case ']':
    return ']';
    case 'a':
    return 'a';
    case 's':
    return 's';
    case 'd':
    return 'd';
    case 'f':
    return 'f';
    case 'g':
    return 'g';
    case 'h':
    return 'h';
    case 'j':
    return 'j';
    case 'k':
    return 'k';
    case 'l':
    return 'l';
    case ';':
    return ';';
    case '@':
    return '\'';
    case '#':
    return '#';
    case 'z':
    return 'z';
    case 'c':
    return 'c';
    case 'v':
    return 'v';
    case 'b':
    return 'b';
    case 'n':
    return 'n';
    case 'm':
    return 'm';
    case ',':
    return ',';
    case '.':
    return '.';
    case 'x':
    return 'x';
    case '/':
    return '/';
    case '|':
    return '\\';
  }

}
